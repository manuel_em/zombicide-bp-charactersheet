$(function () {
	$(".skill-field").autocomplete({
		source : skills,
		position : {
			my : "left bottom-10",
			at : "left top"
		},
		minLength : 0
	}).focus(function () {
		$(this).autocomplete("search", $(this).val());
	});
	$(".export-btn").click(function () {
		var data = {
				skill1 : $("#sheet-skill-blue").val(),
				skill2 : $("#sheet-skill-yellow").val(),
				skill3 : $("#sheet-skill-orange1").val(),
				skill4 : $("#sheet-skill-orange2").val(),
				skill5 : $("#sheet-skill-red1").val(),
				skill6 : $("#sheet-skill-red2").val(),
				skill7 : $("#sheet-skill-red3").val(),
				name : $("#character-name").val()
			}, i,
			loc = window.location,
			url, tab;
		for (i in data) {
			if (!data[i]) {
				alert("Le nom et les 7 compétences sont obligatoires.");
				break;
			}
		}
		url = loc.origin + loc.pathname + "/create?" + $.param(data);
		tab = window.open(url, "exportBGT");
		if (tab) {
			tab.focus();
		}
	});
	$("#clovis-btn").click(function () {
		$("#sheet-skill-blue").val("+1 dé : Mêlée");
		$("#sheet-skill-yellow").val("+1 Action");
		$("#sheet-skill-orange1").val("+1 Action Mêlée gratuite");
		$("#sheet-skill-orange2").val("Maître d'armes");
		$("#sheet-skill-red1").val("+1 Action de Combat gratuite");
		$("#sheet-skill-red2").val("+1 au résultat du dé : Combat");
		$("#sheet-skill-red3").val("Fuite");
		$("#character-name").val("Clovis");
	});
});