var routes = {},
	SheetImage = require("../classes/SheetImage"),
	conf = require(__dirname + "/../conf"),
	exportDir = __dirname + "/../data/export/";

routes.index = function (req, res) {
	res.render(conf.viewspath + "index", {});
};

routes.create = function (req, res) {
	var query = req.query,
		options = {
			name : query.name,
			skills : []
		},
		image, i;
	for (i = 1; i <= 7; i++) {
		options.skills.push(query["skill"+i]);
	}
	image = new SheetImage(options);
	image.write(function (err) {
		if (err) {
			console.error(err);
			return;
		}
		//res.send(exportDir + "test.png");
		res.sendfile("test.png", {root : exportDir});
	});
};

module.exports = routes;