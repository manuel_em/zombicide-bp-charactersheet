/**
 * Module dependencies.
 */

var express = require('express'),
	routes = require('./routes/index'),
	http = require('http'),
	path = require('path'),
	exphbs = require("express-handlebars");

//mongoose = require('mongoose');

var hbs = exphbs.create({
	extname : ".hbs",
	layoutsDir : __dirname + "/views/layouts",
	partialsDir : __dirname + "/views/partials",
	helpers : {
		"loginlink" : function (text, from) {
			return '<a href="/user/login?from=' + from + '">' + text + '</a>';
		}
	}
});

var app = express();

// all environments
app.set('port', process.env.PORT || 3008);
app.set('views', path.join(__dirname, 'views'));
app.engine(".hbs", hbs.engine);
app.set("view engine", ".hbs");
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.secretKey = "ZBP-cs-tEstS";
app.use(express.cookieParser(app.secretKey));
app.sessionStore = new express.session.MemoryStore({ reapInterval: 60000 * 10 });
app.use(express.session({
	secret : app.secretKey,
	store : app.sessionStore,
	expires : new Date(Date.now()+96*3600*1000)
}));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/index', routes.index);

/*
// utilisateur
var User = require('bgt-user');
User.init(app, "/user");
*/
// hon army builder
var ZBPcs = require('../index');
ZBPcs.init(app, {
	engine : express
});
//ZBPcs.registerHelpers(hbs);

http.createServer(app).listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
});
/*
// db connect, mongoose var must be global
mongoose.connect('mongodb://localhost/test-bp-charsheet', function(err) {
	if (err) { throw err; }
});*/