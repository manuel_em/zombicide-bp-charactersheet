var routes = {};

routes.index = function (req, res) {
	var data = {
		title : "Zombicide Black Plague Character Sheet Creator"
	};
	res.render("index", data);
};

module.exports = routes;