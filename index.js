var conf = require("./conf"),
	routesBuild = require("./routes/build"),
	routesVarious = require("./routes/various");

var Module = {

	init : function (app, options) {
		if (!options) options = {};
		if ("modulepath" in options) {
			conf.modulepath = options.modulepath;
		}

		if ("engine" in options) {
			//app.use(conf.modulepath + "/static/data", options.engine.static(conf.staticDir));
			app.use(conf.modulepath + "/static", options.engine.static(__dirname + '/static'));
		}

		app.get(conf.modulepath, routesVarious.index);
		app.get(conf.modulepath + "/create", routesVarious.create);
		app.get(conf.modulepath + "/build", routesBuild.index);
	}
};

module.exports = Module;