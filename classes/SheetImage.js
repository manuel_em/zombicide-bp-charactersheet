var im = require("gm").subClass({imageMagick : true}),
	dataDir = __dirname + "/../data/",
	imagesDir = dataDir + "images/",
	exportDir = dataDir + "export/",
	fontsDir = dataDir + "fonts/",
	fontName = fontsDir + "/Algo_FY_Regular.ttf",
	fontNameSize = 40,
	fontSkill = fontsDir + "/Arek_Italic.otf",
	fontSkillSize = 32;

var SheetImage = function (options) {
	this.setOptions(options);
	this.create();

};

SheetImage.prototype = {

	image : null,
	options : null,

	create : function () {
		this.image = im();
		this.addBackground();
		this.addSkillsBackground();
		this.addNameBackground(this.options.nameSize);
		this.addName(this.options.name);
		for (var i = 0; i < this.options.skills.length; i++) {
			this.addSkill(i, this.options.skills[i]);
		}
	},

	addImage : function (options) {
		if (!options) options = {};
		if (!options.x) options.x = 0;
		if (!options.y) options.y = 0;
		this.image = this.image.in("-page", "+" + options.x + "+" + options.y)
			.in(imagesDir + options.path);
	},

	addName : function (text) {
		this.image = this.image.fill("#4d3828")
			.font(fontName, fontNameSize)
			.drawText(-200, -438, text, "Center");
	},

	addSkill : function (index, skill) {
		var pos = this.getSkillPos(index),
			parts = skill.split("\n");
		this.image = this.image.fill("#1e1a1c")
			.font(fontSkill, fontSkillSize);
		if (parts.length === 1) {
			this.image = this.image.drawText(pos.x, pos.y, skill, pos.origin);
		} else {
			this.image = this.image.drawText(pos.x, pos.y-15, parts[0], pos.origin);
			this.image = this.image.drawText(pos.x, pos.y+15, parts[1], pos.origin);
		}

	},

	getSkillPos : function (index) {
		var x, y, origin;
		if (index < 4) {
			x = 120;
			origin = "NorthWest";
		} else {
			x = 110;
			origin = "NorthEast";
			index++;
		}
		if (index % 4 === 0) y = 662;
		else if (index % 4 === 1) y = 754;
		else if (index % 4 === 2) y = 845;
		else y = 936;
		return {
			x : x,
			y : y,
			origin : origin
		};
	},

	addSimpleImage : function (name) {
		this.addImage({
			path : name + ".png"
		});
	},

	addBackground : function () {
		this.addSimpleImage("bg-sheet");
	},

	addSkillsBackground : function () {
		this.addSimpleImage("bg-skills");
	},

	addNameBackground : function (size) {
		if (!size) size = "short";
		this.addSimpleImage("name-" + size);
	},

	setOptions : function (options) {
		var len;
		this.options = options || {};
		if (!("nameSize" in this.options)) {
			len = this.options.name.length;
			if (len <= 8) {
				this.options.nameSize = "short";
			} else if (len <= 12) {
				this.options.nameSize = "middle";
			} else {
				this.options.nameSize = "large";
			}
		}
	},

	write : function (callback) {
		this.image.mosaic().write(exportDir + "/test.png", callback);
	}
};

module.exports = SheetImage;