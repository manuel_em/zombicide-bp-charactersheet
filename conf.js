module.exports = {
	title : "Zombicide Black Plague charcater sheet creator",
	viewspath : __dirname + "/views/",
	layoutspath : __dirname + "/views/layouts/",
	modulepath : "/zbp-charsheet",
	exportDir : __dirname + "/data/export/",
	staticDir : __dirname + "/data/images/",
	staticpath : "/zbp-charsheet/static",
	locales : ["fr", "en"],
	version : ""
};